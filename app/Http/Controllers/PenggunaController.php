<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserProfile;
use Hash;
use Illuminate\Support\Facades\Auth;
use Session;

class PenggunaController extends Controller
{
    public function simpan_pengguna(Request $request)
    {
        //dd($request->all()); // dump & die untuk semak kewujudan data

        // $semak_pengguna = User::query()->select('no_mykad')->where('no_mykad', $request->no_mykad)->first(); untuk select spesific column dalam table

        $request->validate(
            [
                'nama' => 'required|min:5|max:100|alpha',
                'no_mykad' => 'required|min:12|integer',
                'emel' => 'required|email',
                'password' => 'required|min:12|max:20|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[^@$!%*#?&_]/',
                'no_tel' => 'required',
                // 'tarikhlahir' => 'required'
            ],
            [
                'nama.required' => 'Sila masukkan nama penuh anda.',
                'nama.min' => 'Nama terlalu pendek, minima 5 abjad.',
                'nama.max' => 'Nama terlalu panjang, maksima 100 abjad.',
                'nama.alpha' => 'Nama mestilah dalam abjad',
                'no_mykad.required' => 'Sila masukkan no mykad',
                'password.regex' => 'Katalaluan mestilah kombinasi abjad, nombor & simbol'
            ]
        );

        $semak_pengguna = User::query()->select('no_mykad')
        ->where('no_mykad', $request->no_mykad)
        ->first(); //select all column dalam table

        if($semak_pengguna)
        {
            //dd($semak_pengguna);
            return redirect()->route('login')->with([
                'gagal' => 'Harap maaf, pengguna telah wujud!!!'
            ]);
        }
        else{
            $daftar_pengguna = new User();
            $daftar_pengguna->create([
                'nama' => $request->nama,
                'no_mykad' => $request->no_mykad,
                'emel' => $request->emel,
                'no_tel' => $request->no_tel,
                'password' => Hash::make($request->password),
                'tarikhlahir' => $request->tarikhlahir
            ]);

            // return view('login')->with(['berjaya' => 'Pendaftaran Berjaya. Sila log masuk.']);

            return redirect()->route('login')->with(['berjaya' => 'Pendaftaran Berjaya. Sila log masuk.']);
        }
    }

    public function login(Request $request)
    {

        //dd($request->all());
        $credentials = [
            'no_mykad' => $request->no_mykad,
            'password' => $request->password
        ];

        if(Auth::attempt($credentials))
        {
            //dd(Auth::user()->nama);
            $request->session()->regenerate();

            return redirect()->intended('/layout/master');
            //return view('layouts.master');
        }
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();

        return redirect()->route('login')->with(['berjaya' => 'Anda telah berjaya log keluar']);
    }

    public function kemaskini_simpan(Request $request)
    {
        $user = User::query()->where('no_mykad', $request->no_mykad)->first();

        $userProfile = UserProfile::query()->where('id_user', $user->id)->first();

        if($userProfile)
        {
            $userProfile->update(
                [
                    'jawatan' => $request->jawatan
                ]
            );

        }
        else
        {
            $userProfile = new UserProfile();
            $userProfile->create([
                'id_user' => $user->id,
                'jawatan' => $request->jawatan,
                //'alamat1' => $request->alamat1

            ]);
        }

        $user->update(
            [
                'nama' => $request->nama,
                'emel' => $request->emel,
                'no_mykad' => $request->no_mykad,
                'no_tel' => $request->no_tel
            ]);

        return redirect()->route('profil')->with(['berjaya' => 'Kemaskini berjaya disimpan.']);
    }

    public function senarai_pengguna()
    {
        $senarai_pengguna = User::query()->get();
        $profil_pengguna = UserProfile::query()->get();
        return view('senarai_pengguna', compact('senarai_pengguna','profil_pengguna'));
    }

    public function hapus_pengguna($mykad)
    {
        //dd(decrypt($mykad));

        $check_user = User::query()->where('no_mykad', decrypt($mykad))->first();

        $check_user->delete();

        return redirect()->route('senarai.pengguna');

    }
}
