<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    use HasFactory;

    protected $table = "user_profiles";
    protected $primary = "id";
    public $timestamps = false; //untuk disable timestamp created_at & updated_at
    protected $fillable = [
        'id_user',
        'alamat1',
        'alamat2',
        'poskod',
        'bandar',
        'negeri',
        'jawatan',
        'kementerian'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'id_user','id');
    }
}
