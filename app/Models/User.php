<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = "users"; //kalau guna custom table name
    protected $primary = "id";
    protected $fillable = [
        'nama',
        'emel',
        'no_mykad',
        'password',
        'no_tel',
        'tarikhlahir',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function userProfile()
    {
        return $this->belongsTo(UserProfile::class,'id','id_user');
    }

    // public function perkhidmatan()
    // {
    //     return $this->hasMany(UserProfile::class,'id','id_user');
    // }
}
