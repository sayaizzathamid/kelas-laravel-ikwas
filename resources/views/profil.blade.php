@extends('layouts.master') <!-- include master template -->

@section('style') <!-- include custom style -->
@endsection

@section('kandungan')<!-- include kandungan/content page -->
	<div class="card">
		<div class="card-header">
			<h3>Kemaskini Profil Pengguna</h3>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-12">
					@if($errors->any())
			            @foreach($errors->all() as $error)
			                <div class="alert alert-danger">
			                    {{ $error }}
			                </div>
			            @endforeach
			        @endif
			        @if(session('berjaya'))
			            <div class="alert alert-success">
			                {{session('berjaya')}}
			            </div>
			        @endif
					<form method="post" action="{{ route('kemaskini.simpan') }}">
						@csrf
						<div class="row">
							<div class="col-lg-3">
								<label for="nama">Nama</label>
								<input type="text" name="nama" class="form-control" value="{{ old('nama') ?? Auth::user()->nama ?? '' }}">
							</div>
							<div class="col-lg-3">
								<label for="emel">Emel</label>
								<input type="text" name="emel" class="form-control" value="{{ old('emel') ?? Auth::user()->emel ?? '' }}">
							</div>
							<div class="col-lg-3">
								<label for="no_mykad">MyKad</label>
								<input type="text" name="no_mykad" class="form-control" value="{{ old('no_mykad') ?? Auth::user()->no_mykad ?? '' }}">
							</div>
							<div class="col-lg-3">
								<label for="no_tel">No. Telefon</label>
								<input type="text" name="no_tel" class="form-control" value="{{ old('no_tel') ?? Auth::user()->no_tel ?? '' }}">
							</div>
							<div class="col-lg-3">
								<label for="jawatan">Jawatan</label>
								<input type="text" name="jawatan" class="form-control" value="{{ old('jawatan') ?? Auth::user()->userProfile->jawatan ?? 'Tiada Maklumat' }}">
							</div>

						</div>
						<br>
						<div class="row">
							<div class="col-lg-12">
								<button class="btn btn-success btn-block">Kemaskini</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts') <!-- include custom scripts -->
@endsection
