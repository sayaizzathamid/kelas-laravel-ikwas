<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>BOOKING | Log Masuk</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <style type="text/css">
        .jata{
            width: 50%;
        }
    </style> <!-- untuk custom style/format text/image -->

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <!-- <h1 class="logo-name">IN+</h1> -->
                <img class="jata" src="{{ asset('img/jata.png')}}">

            </div>
            <h3>Sistem Tempahan</h3>
            <p>
                Selamat datang ke Sistem Tempahan IKWAS
            </p>
            {{--@if(session('berjaya'))--}}
                <!-- <div class="alert alert-success"> -->
                    {{-- session('berjaya') --}}
                <!-- </div> -->
            {{-- @endif--}}
            @if(session('gagal'))
                <div class="alert alert-danger">
                    {{ session('gagal') }}
                </div>
            @endif
            <div class="modal fade" tabindex="-1" aria-hidden="true" id="successModal" style="position: absolute; padding-top: 10%; padding-bottom: 10%; display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h3>{{ session('berjaya') }}</h3>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">
                                OK
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <p>Sila Log Masuk.</p>
            <form class="m-t" role="form" method="post" action="{{ route('pengguna.login') }}">
                @csrf
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="No. MyKad" name="no_mykad" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Katalaluan" name="password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Log Masuk</button>

                <a href="#"><small>Lupa Katalaluan?</small></a>
                <p class="text-muted text-center"><small>Belum mempunyai akaun?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{ route('daftar.pengguna') }}">Daftar Akaun</a>
            </form>
            <p class="m-t"> <small>Hak Cipta IKWAS &copy; 2023</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    @if(session('berjaya'))
    <script type="text/javascript">
        $(function(){
            $('#successModal').modal('show');
        });
    </script>
    @endif

</body>

</html>
