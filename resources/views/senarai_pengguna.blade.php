@extends('layouts.master') <!-- include master template -->

@section('style') <!-- include custom style -->
@endsection

@section('kandungan')
	<div class="card">
		<div class="card-header">
			<h3>Senarai Pengguna</h3>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-12">
					<table class="table table-bordered">
						<thead>
							<th>Bil</th>
							<th>Nama</th>
							<th>MyKad</th>
							<th>Emel</th>
							<th>Jawatan</th>
							<th>Tindakan</th>
						</thead>
						<tbody>
							@php
								$i = 1;
							@endphp
							@foreach($senarai_pengguna as $sen_user)
								<tr>
									<td>{{ $i++ }}</td>
									<td>{{ $sen_user->nama }}</td>
									<td>{{ $sen_user->no_mykad }}</td>
									<td>{{ $sen_user->emel }}</td>
									<td>{{ $sen_user->userProfile->jawatan ?? 'Tiada Maklumat'}}</td>
									<td>
										<a href="{{ route('hapus', [encrypt($sen_user->no_mykad)]) }}">
											<button class="btn btn-danger">
												<i class="fa fa-trash"></i> Hapus
											</button>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection