<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>BOOKING | Log Masuk</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <style type="text/css">
        .jata{
            width: 50%;
        }

        .label-align{
            float: left;
        }
    </style> <!-- untuk custom style/format text/image -->

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <!-- <h1 class="logo-name">IN+</h1> -->
                <img class="jata" src="{{ asset('img/jata.png')}}">

            </div>
            <h3>Sistem Tempahan</h3>
            <p>
                Sila lengkapkan maklumat dibawah.
            </p>
            
            @if($errors->any())
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <p>Borang Pendaftaran Pengguna</p>
            <form class="m-t" role="form" action="{{ route('simpan.pengguna') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label class="label-align" for="no_mykad">MyKad</label>
                    <input type="text" class="form-control" placeholder="No. MyKad" name="no_mykad" required="" value="{{ old('no_mykad') ?? '' }}">
                </div>
                <div class="form-group">
                    <label class="label-align" for="nama">Nama Penuh</label>
                    <input type="text" class="form-control" placeholder="Nama Penuh" name="nama" required="" value="{{ old('nama') ?? '' }}">
                </div>
                <div class="form-group">
                    <label class="label-align" for="emel">Emel Rasmi</label>
                    <input type="text" class="form-control" placeholder="emel@contoh.my" name="emel" required="" value="{{ old('emel') ?? '' }}">
                </div>
                <div class="form-group">
                    <label class="label-align" for="no_tel">No. Telefon</label>
                    <input type="text" class="form-control" placeholder="09-xxxxxxx" name="no_tel" required="" value="{{ old('no_tel') ?? '' }}">
                </div>
                <div class="form-group">
                    <label class="label-align" for="tariklahir">Tarikh Lahir</label>
                    <input type="date" class="form-control" placeholder="Tarikh Lahir" name="tariklahir" required="" value="{{ old('tariklahir') ?? '' }}">
                </div>
                <div class="form-group">
                    <label class="label-align" for="password">Katalaluan</label>
                    <input type="password" class="form-control" placeholder="Katalaluan" name="password" required="" value="{{ old('password') ?? '' }}">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Daftar</button>
            </form>
            <p class="m-t"> <small>Hak Cipta IKWAS &copy; 2023</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>

</body>

</html>
