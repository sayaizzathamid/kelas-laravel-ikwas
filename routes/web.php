<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PenggunaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/login', function(){
//     return view('login'); // login adalah nama file dlm folder views
// });

Route::get('/', function(){
    return view('login'); // login adalah nama file dlm folder views
})->name('login');




Route::post('/simpan/pengguna', [PenggunaController::class, 'simpan_pengguna'])->name('simpan.pengguna');

Route::post('/pengguna/login', [PenggunaController::class, 'login'])->name('pengguna.login');

Route::get('/daftar/pengguna', function(){
    return view('daftar');
})->name('daftar.pengguna');



Route::group(['middleware' => ['auth']], function() //untuk pastikan hanya user yang login shj boleh akses page2 ini
{
    Route::get('/layout/master', function(){
        return view('layouts.master'); // master adalah nama file dlm folder views/layouts
    })->name('master');

    Route::get('/pengguna/logkeluar', [PenggunaController::class, 'logout'])->name('logout');

    Route::get('/pengguna/profil/kemaskini', function(){
        return view('profil');
    })->name('profil');

    Route::post('/kemaskini/profil/simpan', [PenggunaController::class, 'kemaskini_simpan'])->name('kemaskini.simpan');

    Route::get('/senarai/pengguna', [PenggunaController::class, 'senarai_pengguna'])->name('senarai.pengguna');

    Route::get('/hapus/pengguna/{mykad}', [PenggunaController::class,'hapus_pengguna'])->name('hapus');
});


